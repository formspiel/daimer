// Ba-Be-D is using 💰('cash');
// https://github.com/fabiospampinato/cash

$(function () {

  $( 'html' ).removeClass ( 'no-js' );

  $( '#nav-mobile--btn' ).on( 'click', function() {
    $( '#nav-mobile--wrapper' ).toggleClass( 'js-open' );

    if ($( this ).attr( 'aria-expanded' ) == 'true') {
      $( this ).attr( "aria-expanded", "false" );
    } else {
      $( this ).attr( "aria-expanded", "true" );
    }
  });

});
