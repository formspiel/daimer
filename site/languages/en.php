<?php
/**
 * Options: https://getkirby.com/docs/guide/languages/introduction
 */

return [
    'code' => 'en',
    'default' => false,
    'direction' => 'ltr',
    'locale' => [
        'en_GB'
    ],
    'name' => 'English',
    'translations' => [
        'logo-label' => 'Zur Startseite der Ba-Be-D Daimer GmbH',
        'nav-lang-label' => 'Language',
        'nav-main-label' => 'Main',
        'nav-breadcrump-label' => 'Breadcrump',
        'old-browser-hint' => 'You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.',
        'alternate-page-description' => 'Your competent partner for track cleaning, measuring devices and measuring services.',
        'mail-us-title' => 'We\'re looking forward to your e-mail!',
        'social-media-linkedin' => 'Visit our LinkedIn page!',
    ],
    'url' => NULL
];