<?php
/**
 * Options: https://getkirby.com/docs/guide/languages/introduction
 */

return [
    'code' => 'de',
    'default' => true,
    'direction' => 'ltr',
    'locale' => [
        'de_DE'
    ],
    'name' => 'Deutsch',
    'translations' => [
        'logo-label' => 'Zur Startseite der Ba-Be-D Daimer GmbH',
        'nav-skip-name' => 'Schnellnavigation',
        'nav-skip-main' => 'Zum Hauptinhalt',
        'nav-lang-label' => 'Sprache wechseln',
        'nav-main-label' => 'Hauptmenü',
        'nav-breadcrump-label' => 'Navigationspfad',
        'old-browser-hint' => 'Sie nutzen einen <strong>veralteten</strong> Browser. Bitte <a href="https://browsehappy.com/">aktualisieren Sie </a> für verbesserte Sicherheit.',
        'alternate-page-description' => 'Ihr kompetenter Partner für Schienenreinigung, Messgeräte und Messdienstleistung.',
        'mail-us-title' => 'Wir freuen uns auf Ihre Nachricht!',
        'social-media-linkedin' => 'Besuchen Sie uns auf LinkedIn!',
    ],
    'url' => NULL
];