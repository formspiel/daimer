  <footer class="footer">
    <a href="<?= url() ?>">&copy; <?= date('Y') ?> <?= $page = page('firmendaten')->addr_company() ?></a>
    <a href="https://www.linkedin.com/company/ba-be-d-daimer-gmbh/"><?= t('social-media-linkedin') ?></a>
    <hr />
    <?php
    /*
     * Custom menu with specific pages
     *
     */
      $items = $pages->find('legal-notice', 'data-privacy', 'support');
      if($items and $items->isNotEmpty()):
    ?>
    <nav>
      <ul class="reset-list">
        <?php foreach($items as $item): ?>
        <li><a href="<?= $item->url() ?>" <?= e($item->isOpen(), ' aria-current="location"') ?>><?= $item->title()->html() ?></a></li>
        <?php endforeach ?>
      </ul>
    </nav>
    <?php endif ?>
    <hr />
    <?php snippet('fsp-debug') ?>
    <?php snippet('incl_address') ?>
  </footer>
</div> <!-- .page-wrapper -->

</body>
</html>
