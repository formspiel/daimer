<?php
	// main menu items
	$items = $pages->listed();

	// only show the menu if items are available
	if($items->isNotEmpty()):
?>

<nav aria-label="<?= t('nav-main-label') ?>" class="nav-main">
	<ul>
	<?php foreach($items as $item): ?>
		<li><a<?php e($item->isOpen(), ' aria-current="page"') ?> href="<?= $item->url() ?>"><?= $item->title()->html() ?></a></li>
	<?php endforeach ?>
	</ul>
</nav>
<?php endif ?>