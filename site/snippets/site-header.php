<?php snippet('a11y-skiplinks') ?>
<?php snippet('nav-main-mobile') ?>
<div class="page-wrapper">
  <header>
    <?php snippet('nav-lang') ?>
    <a class="logo" href="<?= $site->url() ?>" title="<?= t('logo-label') ?>">
      <?= asset('assets/ba-be-d-logo.svg')->read() ?>
    </a>
    <?= asset('assets/rail-top.svg')->read() ?>
    <?= asset('assets/rail-left.svg')->read() ?>
    <?php snippet('nav-main') ?>
    <?php snippet('page-slideshow') ?>
  </header>
  
  <!--  -->
  <!-- <?php snippet('nav-main-sub') ?> -->
