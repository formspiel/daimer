<div class="slideshow--wrapper">
  <?php if ($image = $page->images()->first()): ?>
  <ul class="slideshow" aria-hidden="true">
    <?php foreach ($page->images() as $image): ?>
      <li><figure><img srcset="<?= $image->srcset() ?>" alt="<?= $image->alt() ?>" /><?php if ($image->caption()->isNotEmpty()): ?><figcaption><?= $image->caption() ?></figcaption><?php endif ?></figure></li>
    <?php endforeach ?>
  </ul>
  <?php else: ?>
    <figure class="slideshow--backup"><img srcset="<?= url('assets/placeholder.png') ?>" alt="" /><figcaption>Ups, da haben wir etwas vergessen</figcaption></figure>
  <?php endif ?>
</div>