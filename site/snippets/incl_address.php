<?php $page = page('firmendaten'); ?>

<?php
	/*
	 * Remove all characters except the figures & replace the first "0" with "+49"
	 */
	$addr_phone_code = preg_replace('/[^0-9]/', '', $page->addr_phone() );
	
	if($addr_phone_code[0] == "0"){
		$addr_phone_code = substr_replace($addr_phone_code,'+49',0,1);
	}
?>

<div class="address-wrapper">
	<span><?= $page->addr_company() ?></span>
	<?= asset('assets/icon-rail-profile.svg')->read() ?>
	<span><?= $page->addr_street() ?></span>
	<svg xmlns="http://www.w3.org/2000/svg" width="6" height="8" viewBox="0 0 6 8">
		<use xlink:href="#icon-rail-profile" />
	</svg>
	<span><?= $page->addr_city() ?></span>
	<svg xmlns="http://www.w3.org/2000/svg" width="6" height="8" viewBox="0 0 6 8">
		<use xlink:href="#icon-rail-profile" />
	</svg>
	<span><?= $page->addr_coutry() ?></span>
	<svg xmlns="http://www.w3.org/2000/svg" width="6" height="8" viewBox="0 0 6 8">
		<use xlink:href="#icon-rail-profile" />
	</svg>
	<span><a href="tel:<?= $addr_phone_code ?>"><?= $page->addr_phone() ?></a></span>
	<svg xmlns="http://www.w3.org/2000/svg" width="6" height="8" viewBox="0 0 6 8">
		<use xlink:href="#icon-rail-profile" />
	</svg>
	<span><?= $page->addr_fax() ?></span>
	<span><?= Html::email($page->addr_email(), null , ['title' => t('mail-us-title')]) ?></span>
	<span><a href="<?= $page->addr_url() ?>"><?= $page->addr_url() ?></a></span>
	
	<?= $page->legal_notice_ceo() ?>
	<?= $page->legal_notice_taxid() ?>
	<?= $page->legal_notice_register() ?>
	<?= $page->legal_notice_registernumber() ?>
</div>