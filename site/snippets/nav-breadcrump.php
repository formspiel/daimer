<?php
/* Optimised example of:
 * https://getkirby.com/docs/reference/objects/site/breadcrumb#example
 * saved for later: inline output <?= e($crumb->isActive(), ' class="current"') ?>
 */
?>

<nav aria-label="<?= t('nav-breadcrump-label') ?>" class="nav-breadcrumb">
	<ol>
	<?php foreach($site->breadcrumb() as $crumb): ?>
		<li>
			<?php if ($crumb->isActive()): ?>
				<a aria-current="location"><?= html($crumb->title()) ?></a>
			<?php else: ?>
				<a href="<?= $crumb->url() ?>"><?= html($crumb->title()) ?></a>
			<?php endif ?>
		</li>
	<?php endforeach ?>
	</ol>
</nav>
