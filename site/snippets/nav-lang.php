<nav aria-label="<?= t('nav-lang-label') ?>" class="nav-lang">
	<ul>
	<?php foreach($kirby->languages() as $language): ?>
		<li><a<?php e($kirby->language() == $language, ' aria-current="true"') ?> href="<?= $language->url() ?>" title="<?= html($language->name()) ?>" lang="<?= $language->code() ?>" hreflang="<?= $language->code() ?>"><?= html($language->code()) ?></a></li>
	<?php endforeach ?>
	</ul>
</nav>