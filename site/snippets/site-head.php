<!doctype html>
<html class="no-js" lang="<?= kirby()->language()->code(); ?>">
<head>
  <meta charset="utf-8">
  <?php if ($page->isHomePage()): ?>
  <title><?= $site->title() ?></title>
  <?php else: ?>
  <!-- TODO: add field "seoTitle" to blueprints -->
  <title><?= $page->seoTitle()->or($page->title()) ?> – <?= $site->title() ?></title>
  <?php endif ?>
  
  <meta name="description" content="<?= $page->description()->or(t('alternate-page-description')) ?>">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  
  <?php foreach($kirby->languages() as $language): ?>
    <link rel='alternate' href='<?= $language->url() ?>' hreflang='<?= $language->code() ?>-<?= kirby()->language()->code(); ?>'>
  <?php endforeach ?>
  
  <!-- TODO: Favicon https://github.com/h5bp/html5-boilerplate/tree/master/src -->
  <link rel="icon" type="image/svg+xml" href="<?= url('assets/favicon.svg') ?>">
  <link rel="alternate icon" href="<?= url('assets/favicon.ico') ?>">
  <link rel="mask-icon" href="<?= url('assets/safari-pinned-tab.svg') ?>" color="#005D97">
  <meta name="theme-color" content="#005D97">
  <meta name="msapplication-navbutton-color" content="#005D97">

  <?= css(['assets/daimer-min.css', '@auto']) ?>
  <!-- Autoloading template specific css files -->  
    <?= css('@auto') ?>
  <?= js([
    'https://cdnjs.cloudflare.com/ajax/libs/cash/8.1.0/cash.min.js',
    'assets/daimer-min.js',
  ]) ?>
</head>
<body>
<!--[if IE]>
  <div class="browserupgrade"><?= t('old-browser-hint'); ?></div>
<![endif]-->