<?php
	// independent sub menu –> https://getkirby.com/docs/cookbook/templating/menus#sub-menus
	$items = false;

	// get the open item on the first level
	if($root = $pages->findOpen()) {
		// get visible children for the root item
		$items = $root->children()->listed();
	}

	// only show the menu if items are available
	if($items and $items->isNotEmpty()):
?>

<nav aria-label="<?= t('nav-sub-label') ?>" class="nav-main-sub">
	<ul>
	<?php foreach($items as $item): ?>
		<li><a<?php e($item->isOpen(), ' aria-current="page"') ?> href="<?= $item->url() ?>"><?= $item->title()->html() ?></a></li>
	<?php endforeach ?>
	</ul>
</nav>
<?php endif ?>