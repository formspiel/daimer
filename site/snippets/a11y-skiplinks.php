<nav aria-label="<?= t('nav-skip-name') ?>" id="skiplinks">
  <ul>
    <li><a href="#nav-lang"><?= t('nav-lang-label') ?></a></li>
    <li><a href="#main"><?= t('nav-skip-main') ?></a></li>
  </ul>
</nav>