<?php
	// nested menu –> https://getkirby.com/docs/cookbook/templating/menus#nested-menu
	$items = $pages->listed();

	// only show the menu if items are available
	if($items->isNotEmpty()):
?>
<button id="nav-mobile--btn" aria-expanded="false" aria-controls="nav-mobile--wrapper"><?= t('nav-main-label') ?></button>

<nav aria-label="<?= t('nav-main-label') ?>" id="nav-mobile--wrapper">
	<ul>
	<?php foreach($items as $item): ?>
		<li><a<?php e($item->isActive(), ' aria-current="page"') ?> <?php e($item->isOpen(), ' class="parent"') ?> href="<?= $item->url() ?>"><?= $item->title()->html() ?></a>
		
		<?php
			// get all children for the current menu item
			$children = $item->children()->listed();
			
			// display the submenu if children are available
			if($children->isNotEmpty()):
		?>
		
		<ul>
		<?php foreach($children as $child): ?>
			<li><a<?php e($child->isActive(), ' aria-current="page"') ?> href="<?= $child->url() ?>"><?= $child->title()->html() ?></a></li>
		<?php endforeach ?>
		</ul>
		<?php endif ?>
		
		</li>
	<?php endforeach ?>
	</ul>
	<div class="nav-mobile--links">
		<a href="">Fon</a>
		<a href="mailto:info@daimer.info">E-Mail</a>
		<a href="#">Navi</a>
</nav>
<?php endif ?>