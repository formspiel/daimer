<?php snippet('site-head') ?>

<?php snippet('site-header') ?>

<main>
  <?php snippet('nav-breadcrump') ?>
  <?php snippet('intro') ?>
	
  <h1><?= $page->page_headline() ?></h1>
  
  <?php foreach ($page->layout()->toLayouts() as $layout): ?>
  <section class="grid" id="<?= $layout->id() ?>">
    <?php foreach ($layout->columns() as $column): ?>
    <div class="column" style="--span:<?= $column->span() ?>">
      <div class="blocks">
        <?= $column->blocks() ?>
      </div>
    </div>
    <?php endforeach ?>
  </section>
  <?php endforeach ?>
  
  <hr />

<!-- TODO: No Output -->
<?php foreach ($page->myBlocksField()->toBlocks() as $block): ?>
<div id="<?= $block->id() ?>" class="block block-type-<?= $block->type() ?>">
  <?= $block ?>
</div>
<?php endforeach ?>

<?php $items = $page->teaser_pages()->toStructure(); ?>
<div class="teaser-pages--wrapper">
<h3>[T] Unsere Lösungen für die Bereiche:</h3>
<ul>
<?php foreach ($items as $item): ?>
<li>
  <a href="<?= $link = $item->pages_link()->toLinkObject() ?>">
    <?php if($image = $item->teaser_pages_img()->toFile()): ?><img src="<?= $image->crop(80)->url() ?>" alt=""><?php endif ?>
    <?= $item->teaser_pages_text()->html() ?>
  </a>
</li>
<?php endforeach ?>
</ul>
</div>

<?php if ($page->teaser_01()->toBool() === true) { ?>
  <p>✅ Teamviewer</p>
<?php } else { 
  // do that
}?>

<?php if ($page->teaser_02()->toBool() === true) { ?>
  <p>✅ Teaser 2</p>
<?php } else { 
  // do that
}?>

</main>

<?php snippet('site-footer') ?>