<?php
/**
 * All config options: https://getkirby.com/docs/reference/system/options
 * Sitemap: https://getkirby.com/docs/cookbook/content/sitemap
 */
 
return [
    'debug' => true,
    'languages' => true,
    'languages.detect' => true,
    'sitemap.ignore' => ['error'], /* exclude error page from sitemap */
    
    'routes' => [
      [
        'pattern' => 'sitemap.xml',
        'action'  => function() {
            $pages = site()->pages()->index();
    
            // fetch the pages to ignore from the config settings,
            // if nothing is set, we ignore the error page
            $ignore = kirby()->option('sitemap.ignore', ['error']);
    
            $content = snippet('sitemap', compact('pages', 'ignore'), true);
    
            // return response with correct header type
            return new Kirby\Cms\Response($content, 'application/xml');
        }
      ],
      [
        'pattern' => 'sitemap',
        'action'  => function() {
          return go('sitemap.xml', 301);
        }
      ]
    ],
    
    'thumbs' => [
        'srcsets' => [
            'default' => [300, 800, 1024],
            'cover' => [800, 1024, 2048]
        ]
    ]

/**
 * https://getkirby.com/docs/guide/authentication/login-methods    
 *   'auth' => [
 *       'methods' => ['password' => ['2fa' => true]], 'code']
 *  ODER
         'methods' => ['password', 'password-reset']
 *  ODER
         'methods' => ['password', 'code']
 *   ]
 */
];
